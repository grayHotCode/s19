/*
ES6
	ECMAScript
		- a standard for scripting languages like JavaScript
		- ES5 
			- current version implemented into most browsers
	1. let and const
	2. Destructuring
		- allows us to break apart key structure into variable
		- Array Destructuring
*/
let employee = ['Sotto', 'Tito', 'Senate President', 'Male'];

let [lastName, firstName, position, gender] = employee;//one to one mapping

console.log(lastName);
console.log(position);

let player2 = ['Curry', 'Lillard', 'Paul', 'Irving'];

const [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = player2;
console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

// object destructuring - it allows us to destructure an object by allowing to add the values of an object's property into respective variables

let pet = {
	species: 'dog',
	color: 'brown',
	breed: 'German Sheperd'
};

// let {breed, color} = pet;
// alert(`I have ${breed} dog and it's color is ${color}`);
getPet({
	species: 'dog',
	color: 'black',
	breed: 'Doberman'
})


// function getPet(options){
// 	alert(`I have ${options.breed} dog and it's color is ${options.color}`);
// };

// function getPet(options){
// 	let breed = options.breed;
// 	let color = options.color;

// 	console.log(`I have ${breed} dog and it's color is ${color}`);
// };


// function getPet(options){
// 	let {breed, color} = options;
// 	console.log(`I have ${breed}dog and it's color is ${color}`);
// }

function getPet({breed, color}){
	console.log(`I have ${breed}dog and it's color is ${color}`);
};

// without function
let getPet2 = {
	species: 'dog',
	color: 'brown',
	breed: 'German Sheperd'
};

let {breed, color} = getPet2;
console.log(`I have ${breed}dog and it's color is ${color}`);

/*
	let person = {
		name: <name>,
		birthday: <bday>,
		age: <age>
	}

	Create 3 new sentence variable which will contain the following strings:
		sentence1: Hi, I'm <personName>,
		sentence2: I was born on <personbday>,
		sentence3: I am <personage> years old

	Display the three sentences in console or in alert
 */





let person = {
	name: 'Peter Parker',
	birthday: 'May 20, 1985',
	age: 36
};

let sentence1 = `Hi, I,m ${name}`;
let sentence2 = `I was born on ${birthday}`;
let sentence3 = `I am ${age} years old`;

	console.log(sentence1);
	console.log(sentence2);
	console.log(sentence3);
