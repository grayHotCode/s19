
let student1 = {
	name: "Shawn Michaels", 
	birthday: "May 5, 2003", 
	age: 18, 
	isEnrolled: true,
	class: ["Philosphy 101", " Social Sciences 201"],
	introduce1: () =>{
		console.log(`Hi, I'm ${student1.name}. I am ${student1.age} years old`);
		console.log(`I study the following courses " ${student1.class}`);
	}
	
};

let student2 = {
	name: "Steve Austin", 
	birthday: "June 15, 2001", 
	age: 20, 
	isEnrolled: true,
	class: ["Philosphy 401", " Natural Sciences 402"],
	introduce2: () =>{
		console.log(`Hi, I'm ${student2.name}. I am ${student2.age} years old`);
		console.log(`I study the following courses " ${student2.class}`);
	}
	
};

student1.introduce1();
student2.introduce2();


class Dog{
	constructor(name, breed, dogAge){
		this.name = name;
		this.breed = breed;
		this.dogAge = dogAge * 7;
	}
};
let dog1 = new Dog('HatDog', 'Golden Retriever', 5);
console.log(dog1);

let dog2 = new Dog('ColdDog', 'Siberian Husky', 4);
console.log(dog2);



function getCube(num){

	console.log(Math.pow(num,3));

}

let cube = getCube(3);

console.log(cube)

let numArr = [15,16,32,21,21,2]

numArr.forEach(function(num){

	console.log(num);
})

let numSquared = numArr.map(function(num){

	return num ** 2;

})

console.log(numSquared);










